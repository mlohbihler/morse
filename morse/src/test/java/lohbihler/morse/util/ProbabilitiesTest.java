package lohbihler.morse.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ProbabilitiesTest {
    @Test
    public void normalize() {
        final Probabilities<String> probs = new Probabilities<>();
        probs.add("a", 0.1);
        probs.add("b", 0.1);
        probs.add("c", 0.3);
        probs.normalize();

        assertEquals(0.2, probs.getProbability("a"), 0.001);
        assertEquals(0.2, probs.getProbability("b"), 0.001);
        assertEquals(0.6, probs.getProbability("c"), 0.001);
    }
}
