package lohbihler.morse.gen;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

import org.junit.Test;

public class EncoderTest {
    @Test
    public void fullTest() {
        final Recorder recorder = new Recorder();
        final Encoder encoder = new Encoder(1, 0, 0, 0, recorder, null);
        encoder.encode(" a bcd  e  ");
        assertEquals(28, recorder.events.size());
        assertEquals(new EncoderEvent(0, true), recorder.events.get(0));
        assertEquals(new EncoderEvent(1, false), recorder.events.get(1));
        assertEquals(new EncoderEvent(1, true), recorder.events.get(2));
        assertEquals(new EncoderEvent(3, false), recorder.events.get(3));
        assertEquals(new EncoderEvent(7, true), recorder.events.get(4));
        assertEquals(new EncoderEvent(3, false), recorder.events.get(5));
        assertEquals(new EncoderEvent(1, true), recorder.events.get(6));
        assertEquals(new EncoderEvent(1, false), recorder.events.get(7));
        assertEquals(new EncoderEvent(1, true), recorder.events.get(8));
        assertEquals(new EncoderEvent(1, false), recorder.events.get(9));
        assertEquals(new EncoderEvent(1, true), recorder.events.get(10));
        assertEquals(new EncoderEvent(1, false), recorder.events.get(11));
        assertEquals(new EncoderEvent(3, true), recorder.events.get(12));
        assertEquals(new EncoderEvent(3, false), recorder.events.get(13));
        assertEquals(new EncoderEvent(1, true), recorder.events.get(14));
        assertEquals(new EncoderEvent(1, false), recorder.events.get(15));
        assertEquals(new EncoderEvent(1, true), recorder.events.get(16));
        assertEquals(new EncoderEvent(3, false), recorder.events.get(17));
        assertEquals(new EncoderEvent(1, true), recorder.events.get(18));
        assertEquals(new EncoderEvent(1, false), recorder.events.get(19));
        assertEquals(new EncoderEvent(3, true), recorder.events.get(20));
        assertEquals(new EncoderEvent(3, false), recorder.events.get(21));
        assertEquals(new EncoderEvent(1, true), recorder.events.get(22));
        assertEquals(new EncoderEvent(1, false), recorder.events.get(23));
        assertEquals(new EncoderEvent(1, true), recorder.events.get(24));
        assertEquals(new EncoderEvent(1, false), recorder.events.get(25));
        assertEquals(new EncoderEvent(7, true), recorder.events.get(26));
        assertEquals(new EncoderEvent(1, false), recorder.events.get(27));
    }

    @Test
    public void blank() {
        final Recorder recorder = new Recorder();
        final Encoder encoder = new Encoder(1, 0, 0, 0, recorder, null);
        encoder.encode("   ");
        assertEquals(0, recorder.events.size());
    }

    @Test
    public void unhandledCharacter() {
        final Recorder recorder = new Recorder();
        final Encoder encoder = new Encoder(1, 0, 0, 0, recorder, null);
        encoder.encode("%");
        assertEquals(0, recorder.events.size());
    }

    @Test
    public void unhandledCharacters1() {
        final Recorder recorder = new Recorder();
        final Encoder encoder = new Encoder(1, 0, 0, 1000, recorder, null);
        encoder.encode(" % a% ");
        assertEquals(4, recorder.events.size());
        assertEquals(new EncoderEvent(1000, true), recorder.events.get(0));
        assertEquals(new EncoderEvent(1, false), recorder.events.get(1));
        assertEquals(new EncoderEvent(1, true), recorder.events.get(2));
        assertEquals(new EncoderEvent(3, false), recorder.events.get(3));
    }

    @Test
    public void unhandledCharacters2() {
        final Recorder recorder = new Recorder();
        final Encoder encoder = new Encoder(1, 0, 0, 0, recorder, null);
        encoder.encode("%e%");
        assertEquals(2, recorder.events.size());
        assertEquals(new EncoderEvent(0, true), recorder.events.get(0));
        assertEquals(new EncoderEvent(1, false), recorder.events.get(1));
    }

    static class Recorder implements BiConsumer<Integer, Boolean> {
        final List<EncoderEvent> events = new ArrayList<>();

        @Override
        public void accept(final Integer time, final Boolean signal) {
            //            System.out.println(time + "\t" + (signal ? "+" : "-"));
            events.add(new EncoderEvent(time, signal));
        }
    }

    static class EncoderEvent {
        final Integer time;
        final Boolean value;

        public EncoderEvent(final Integer time, final Boolean value) {
            this.time = time;
            this.value = value;
        }

        @Override
        public String toString() {
            return "EncoderEvent [time=" + time + ", value=" + value + "]";
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + (time == null ? 0 : time.hashCode());
            result = prime * result + (value == null ? 0 : value.hashCode());
            return result;
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            final EncoderEvent other = (EncoderEvent) obj;
            if (time == null) {
                if (other.time != null)
                    return false;
            } else if (!time.equals(other.time))
                return false;
            if (value == null) {
                if (other.value != null)
                    return false;
            } else if (!value.equals(other.value))
                return false;
            return true;
        }
    }
}
