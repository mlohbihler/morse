package lohbihler.morse.gen;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

import org.apache.commons.io.IOUtils;

public class Generator {
    public static void main(final String[] args) throws Exception {
        //        encode("input/longQuote.txt", 200, 0, 0, 1000, "input/encodedLongQuote1.txt");
        //        encode("input/longQuote.txt", 200, 0, 0.1, 1000, "input/encodedLongQuote2.txt");
        //        encode("input/longQuote.txt", 200, 5, 0.1, 1000, "input/encodedLongQuote3.txt");
        encode("input/longQuote.txt", 200, 35, 0.1, 1000, "input/encodedLongQuote4.txt");
        //        encode("input/quote.txt", 200, 0, 0, 1000, "input/encodedQuote1.txt");
        //        encode("input/quote.txt", 200, 0, 0.1, 1000, "input/encodedQuote2.txt");
        //        encode("input/quote.txt", 200, 5, 0.1, 1000, "input/encodedQuote3.txt");
        //        encode("input/shortQuote.txt", 200, 0, 0, 1000, "input/encodedShortQuote1.txt");
        //        encode("input/shortQuote.txt", 200, 0, 0.1, 1000, "input/encodedShortQuote2.txt");
        //        encode("input/shortQuote.txt", 200, 5, 0.1, 1000, "input/encodedShortQuote3.txt");
    }

    /**
     * Generate a morse code output file from a textual input. The output is in event form, where each line is a moment
     * (specifically the amount of milliseconds since the last event) and a value, which is either '+' (on) or '-'
     * (off). The event moments have decent variability and noise, but the values do not. A more realistic file would
     * output a signal at a regular interval (say, every millisecond), and the signal would be a float instead of a
     * boolean.
     *
     * @param source
     *            text source file
     * @param beatMs
     *            the ms of a beat, i.e. a dot or a silence between symbols
     * @param stdDev
     *            the ms of the standard deviation beat variation. Pretty much just beat length noise.
     * @param beatChangeProbability
     *            the probability that the beat will change. Becomes a random walk in the beat length.
     * @param startTime
     *            delay before starting the signal
     * @param outputFile
     *            the file to which to write
     * @throws Exception
     */
    static void encode(final String source, final int beatMs, final int stdDev, final double beatChangeProbability,
            final int startTime, final String outputFile) throws Exception {
        String content;
        try (final FileReader input = new FileReader(source)) {
            content = IOUtils.toString(input);
        }

        try (final PrintWriter out = new PrintWriter(new FileWriter(outputFile))) {
            out.println("! source = " + source);
            out.println("! beatNs = " + beatMs);
            out.println("! stdDev = " + stdDev);
            out.println("! beatChangeProbability = " + beatChangeProbability);
            out.println("! startTime = " + startTime);

            final Encoder encoder = new Encoder(beatMs, stdDev, beatChangeProbability, startTime,
                    (time, signal) -> out.println(time + "\t" + (signal ? "+" : "-")),
                    beat -> out.println("! beat change = " + beat));

            encoder.encode(content);
        }
    }
}
