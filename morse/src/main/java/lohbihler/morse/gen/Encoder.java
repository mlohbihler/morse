package lohbihler.morse.gen;

import java.util.Random;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import lohbihler.morse.MorseCode;
import lohbihler.morse.util.Histogram;

public class Encoder {
    // Don't allow time deviations to result in values lower than this.
    private static final int MIN_TIME = 1;

    private int beatMs;
    private final int stdDev;
    private final double beatChangeProbability;
    private final int beatChange;
    private final int minBeat;
    private final int maxBeat;
    private final BiConsumer<Integer, Boolean> signalCallback;
    private final Consumer<Integer> beatChangeCallback;

    private final Random RANDOM = new Random();
    private int time = 0;

    // Write the histogram of the times after generation.
    Histogram histogram = new Histogram(0, 3000, 1);

    public Encoder(final int beatMs, final int stdDev, final double beatChangeProbability, final int startTime,
            final BiConsumer<Integer, Boolean> signalCallback, final Consumer<Integer> beatChangeCallback) {
        this.beatMs = beatMs;
        this.stdDev = stdDev;
        this.beatChangeProbability = beatChangeProbability;
        beatChange = beatMs / 100;
        minBeat = (int) (beatMs * 0.5);
        maxBeat = beatMs * 2;
        time = startTime;
        this.signalCallback = signalCallback;
        this.beatChangeCallback = beatChangeCallback;
    }

    public void encode(final String input) {
        // Replace whitespace with a space
        String content = input.replaceAll("\\s+", " ");
        content = content.trim();

        char lastChar = ' ';
        for (int i = 0; i < content.length(); i++) {
            final char c = content.charAt(i);
            if (c == ' ') {
                if (lastChar != ' ') {
                    writeSymbol(MorseCode.WORD_SEPARATION);
                    lastChar = c;
                }
            } else {
                final MorseCode[] symbols = MorseCode.getSymbols(c);
                if (symbols != null) {
                    if (lastChar != ' ') {
                        writeSymbol(MorseCode.LETTER_SEPARATION);
                    }
                    lastChar = c;

                    MorseCode lastSymbol = null;
                    for (final MorseCode symbol : symbols) {
                        if (lastSymbol != null) {
                            writeSymbol(MorseCode.SYMBOL_SEPARATION);
                        }
                        lastSymbol = symbol;
                        writeSymbol(symbol);
                    }
                }
            }
        }
        if (lastChar != ' ') {
            writeSymbol(MorseCode.SYMBOL_SEPARATION);
        }

        System.out.println(histogram.toExcelString());
    }

    void writeSymbol(final MorseCode symbol) {
        switch (symbol) {
        case DOT:
        case DASH:
            signalCallback.accept(time, true);
            break;
        default:
            signalCallback.accept(time, false);
            break;
        }

        final int t = beatMs * symbol.beats;
        final int var = (int) ((RANDOM.nextGaussian() - 0.5) * stdDev * symbol.beats);
        if (RANDOM.nextDouble() < beatChangeProbability) {
            beatMs += RANDOM.nextBoolean() ? beatChange : -beatChange;
            if (beatMs < minBeat) {
                beatMs = minBeat;
            }
            if (beatMs > maxBeat) {
                beatMs = maxBeat;
            }
            if (beatChangeCallback != null) {
                beatChangeCallback.accept(beatMs);
            }
        }
        time = t + var;
        if (time < MIN_TIME) {
            time = MIN_TIME;
        }

        histogram.add(time);
    }
}
