package lohbihler.morse;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(final String[] args) throws Exception {
        LOG.info("Starting...");

        final FakeClock clock = new FakeClock();
        final AtomicBoolean target = new AtomicBoolean();
        final SignalConsumer consumer = new SignalConsumer(target);

        final SignalChangeListener signalChangeListener = new SignalChangeListener();
        consumer.addListener(signalChangeListener);

        final SymbolChangeListener symbolChangeListener = new SymbolChangeListener();
        signalChangeListener.addListener(symbolChangeListener);

        final LetterChangeListener letterChangeListener = new LetterChangeListener();
        symbolChangeListener.addListener(letterChangeListener);

        //        final String filename = "input/encodedShortQuote3.txt";
        //        final String filename = "input/encodedShortQuote4.txt";
        //        final String filename = "input/encodedQuote3.txt";
        final String filename = "input/encodedLongQuote2.txt";
        //        final String filename = "input/encodedLongQuote3.txt";
        //        final String filename = "input/encodedLongQuote4.txt";
        try (final FileSignalProducer producer = new FileSignalProducer(target, filename)) {
            // Wait for the producer to finish.
            while (!producer.isDone()) {
                clock.advance(10);
                producer.execute(clock);
                consumer.execute(clock);
                signalChangeListener.execute(clock);
            }
        }

        // Run the consumer a few more times.
        for (int i = 0; i < 1000; i++) {
            consumer.execute(clock.advance(10));
            signalChangeListener.execute(clock);
        }

        LOG.info("Done");
        //        System.out.println("On times: " + listener.getOnTimes().toExcelString());
        //        System.out.println("Off times: " + listener.getOffTimes().toExcelString());
    }

    static class FakeClock extends Clock {
        private long now;

        Clock advance(final long diff) {
            now += diff;
            return this;
        }

        @Override
        public long millis() {
            return now;
        }

        @Override
        public ZoneId getZone() {
            return null;
        }

        @Override
        public Clock withZone(final ZoneId zone) {
            return null;
        }

        @Override
        public Instant instant() {
            return null;
        }
    }
}
