package lohbihler.morse;

import java.time.Clock;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lohbihler.morse.util.Centroid;
import lohbihler.morse.util.KMeans1D;
import lohbihler.morse.util.Probabilities;
import lohbihler.morse.util.StandardDeviation;

/**
 * TODO either provide centroid priors, or prefix all encodings with a calibration sequence.
 */
public class SignalChangeListener implements ChangeListener<Boolean> {
    static final Logger LOG = LoggerFactory.getLogger(SignalChangeListener.class);
    static final int MAX_DURATION = 5000;
    static final int MAX_DURATION_COUNT = 100;

    private final MorseCode[] onSymbols = { MorseCode.DOT, MorseCode.DASH };
    private final MorseCode[] offSymbols = { MorseCode.SYMBOL_SEPARATION, MorseCode.LETTER_SEPARATION,
            MorseCode.WORD_SEPARATION };

    private final List<ChangeListener<MorseCode>> listeners = new ArrayList<>();

    private long lastTime;
    private boolean lastValue;
    private final Deque<Integer> onTimes = new LinkedList<>();
    private List<Map.Entry<Centroid, List<Integer>>> onCentroids;
    private final Deque<Integer> offTimes = new LinkedList<>();
    private List<Map.Entry<Centroid, List<Integer>>> offCentroids;

    public void addListener(final ChangeListener<MorseCode> listener) {
        listeners.add(listener);
    }

    @Override
    public void change(final Clock clock, final Boolean on) {
        final long now = clock.millis();

        if (lastTime != 0) {
            final int duration = (int) (now - lastTime);

            if (on) {
                // Determine the symbol that was read.
                if (offCentroids != null) {
                    Probabilities<MorseCode> probs = findCentroidProbabilities(offCentroids, duration, offSymbols);
                    for (final ChangeListener<MorseCode> listener : listeners) {
                        probs = Probabilities.merge(probs, listener.getPredictions(clock));
                    }
                    final MorseCode likely = probs.getMostLikely().getValue();

                    listeners.forEach(l -> l.change(clock, likely));
                }

                offCentroids = saveDuration(offTimes, offCentroids, duration, 3);
            } else {
                // Determine the symbol that was read.
                if (onCentroids != null) {
                    Probabilities<MorseCode> probs = findCentroidProbabilities(onCentroids, duration, onSymbols);
                    for (final ChangeListener<MorseCode> listener : listeners) {
                        probs = Probabilities.merge(probs, listener.getPredictions(clock));
                    }
                    final MorseCode likely = probs.getMostLikely().getValue();

                    listeners.forEach(l -> l.change(clock, likely));
                }

                onCentroids = saveDuration(onTimes, onCentroids, duration, 2);
            }
        }

        lastTime = now;
        lastValue = on;
    }

    @Override
    public Probabilities<Boolean> getPredictions(final Clock clock) {
        // No op. At the bottom level there is no one interested in the predictions (yet).
        return null;
    }

    public void execute(final Clock clock) {
        if (lastTime != 0) {
            final int duration = (int) (clock.millis() - lastTime);
            if (duration > MAX_DURATION) {
                if (lastValue) {
                    // TODO what to do if the signal is on for too long.
                } else {
                    // If the signal is off for too long, emit a silence signal.
                    final int nearest = findNearestCentroid(offCentroids, duration);
                    listeners.forEach(l -> l.change(clock, offSymbols[nearest]));
                    lastTime = 0;
                }
            }
        }

        //        final Map<MorseCode, Double> predictions = getPredictions(clock, lastValue);
        //        if (predictions.isEmpty()) {
        //            LOG.warn("Signal predictions is empty");
        //        }
    }

    public Map<MorseCode, Double> getPredictions(final Clock clock, final boolean on) {
        final Map<MorseCode, Double> predictions = new HashMap<>();
        if (lastTime != 0) {
            final int elapsed = (int) (clock.millis() - lastTime);

            if (on) {
                if (onCentroids != null) {
                    createPredictions(onCentroids, elapsed, predictions, onSymbols);
                }
            } else {
                if (offCentroids != null) {
                    createPredictions(offCentroids, elapsed, predictions, offSymbols);
                }
            }
        }
        return predictions;
    }

    /**
     * NOTE: this way of determining the predictions does not account for elapsed times possibly being longer than the
     * max time for the symbol. For example, if the times for a dot are [9,10,10,10,11], the first time for a dash is
     * 30, and the current elapsed is 12, there is still a possibility that the current symbol could be a dot. But, all
     * dot values will have been filtered out, and so the predictions will return a 0 probability for dot. Perhaps the
     * probabilities can also take into the distance of the elapsed time from the centroids.
     */
    private static void createPredictions(final List<Map.Entry<Centroid, List<Integer>>> centroids, final int elapsed,
            final Map<MorseCode, Double> predictions, final MorseCode... codes) {
        final int counts[] = new int[codes.length];
        int sum = 0;
        for (int i = 0; i < counts.length; i++) {
            sum += counts[i] = count(centroids, i, elapsed);
        }
        if (sum > 0) {
            for (int i = 0; i < counts.length; i++) {
                if (counts[i] > 0) {
                    predictions.put(codes[i], (double) counts[i] / sum);
                }
            }
        }
    }

    private static int count(final List<Map.Entry<Centroid, List<Integer>>> centroids, final int index,
            final int elapsed) {
        if (index >= centroids.size()) {
            return 0;
        }
        return (int) centroids.get(index).getValue().stream().filter(time -> time > elapsed).count();
    }

    private static List<Map.Entry<Centroid, List<Integer>>> saveDuration(final Deque<Integer> times,
            final List<Map.Entry<Centroid, List<Integer>>> centroids, final int duration, final int centroidCount) {
        // Only keep the max duration count of entries in the queue.
        if (times.size() >= MAX_DURATION_COUNT) {
            // Pull from the head.
            times.poll();
        }
        // Add to the tail.
        times.add(duration);
        final Set<Centroid> centroidSet = centroids == null ? null
                : centroids.stream().map(e -> e.getKey()).collect(Collectors.toSet());
        return KMeans1D.minDistClusters(centroidCount, centroidSet, times).entrySet().stream() //
                .sorted((a, b) -> a.getKey().getValue() - b.getKey().getValue()) // Sort values
                .collect(Collectors.toList()); // Convert to list
    }

    private static Probabilities<MorseCode> findCentroidProbabilities(
            final List<Map.Entry<Centroid, List<Integer>>> centroids, final int value, final MorseCode[] codes) {
        final Probabilities<MorseCode> probs = new Probabilities<>();

        final AtomicInteger index = new AtomicInteger(0);
        centroids.stream().map(e -> e.getKey()).forEach(c -> {
            final double prob = StandardDeviation.getValueProbability(c.getValue(), c.getStdDev(), value);
            probs.add(codes[index.get()], prob);
            index.incrementAndGet();
        });

        probs.normalize();

        return probs;
    }

    private static int findNearestCentroid(final List<Map.Entry<Centroid, List<Integer>>> centroids, final int value) {
        int nearest = 0;
        int distance = Math.abs(centroids.get(0).getKey().getValue() - value);
        for (int i = 1; i < centroids.size(); i++) {
            final int d = Math.abs(centroids.get(i).getKey().getValue() - value);
            if (distance > d) {
                distance = d;
                nearest = i;
            }
        }
        return nearest;
    }
}
