package lohbihler.morse;

import java.time.Clock;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SignalConsumer {
    static final Logger LOG = LoggerFactory.getLogger(SignalConsumer.class);

    private final AtomicBoolean input;
    private final List<ChangeListener<Boolean>> listeners = new ArrayList<>();

    private boolean currentValue;

    public SignalConsumer(final AtomicBoolean input) {
        this.input = input;
    }

    public void addListener(final ChangeListener<Boolean> listener) {
        listeners.add(listener);
    }

    public void execute(final Clock clock) {
        // Check for input changes.
        if (input.get() != currentValue) {
            currentValue = input.get();
            final Boolean b = currentValue;
            listeners.forEach(l -> l.change(clock, b));
        }
    }
}
