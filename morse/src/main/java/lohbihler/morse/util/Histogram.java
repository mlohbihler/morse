package lohbihler.morse.util;

public class Histogram {
    private final int[] buckets;
    private final int[] counts;
    private int total;

    public Histogram(final int[] buckets) {
        this.buckets = buckets;
        this.counts = new int[buckets.length + 1];
    }

    public Histogram(final int from, final int to, final int each) {
        final int range = to - from;
        final int count = range / each + 1;
        buckets = new int[count];
        int br = from;
        for (int i = 0; i < count; i++) {
            buckets[i] = br;
            br += each;
        }
        counts = new int[count + 1];
    }

    public void add(final int value) {
        counts[getIndex(value)]++;
        total++;
    }

    public int[] getCounts() {
        return counts;
    }

    public int getCount(final int value) {
        return counts[getIndex(value)];
    }

    private int getIndex(final int value) {
        for (int i = 0; i < buckets.length; i++) {
            if (value < buckets[i]) {
                return i;
            }
        }
        return buckets.length;
    }

    public int getTotal() {
        return total;
    }

    public void dump() {
        for (final int i : counts) {
            System.out.println(i);
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (int i = 0; i < buckets.length; i++) {
            sb.append("<=").append(buckets[i] - 1).append('=').append(counts[i]).append(", ");
        }
        sb.append(">").append(buckets[buckets.length - 1] - 1).append('=').append(counts[buckets.length]);
        sb.append(']');
        return sb.toString();
    }

    public String toExcelString() {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < buckets.length; i++) {
            sb.append("<=").append(buckets[i] - 1).append('\t').append(counts[i]).append('\n');
        }
        sb.append(">").append(buckets[buckets.length - 1] - 1).append('\t').append(counts[buckets.length]).append('\n');
        return sb.toString();
    }
}
