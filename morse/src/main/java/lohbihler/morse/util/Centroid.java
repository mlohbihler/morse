package lohbihler.morse.util;

public class Centroid {
    private final String id;
    private int value;
    private int stdDev;

    public Centroid(final String id, final int value) {
        this.id = id;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(final int value) {
        this.value = value;
    }

    public int getStdDev() {
        return stdDev;
    }

    public void setStdDev(final int stdDev) {
        this.stdDev = stdDev;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (id == null ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Centroid other = (Centroid) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Centroid [id=" + id + ", value=" + value + ", stdDev=" + stdDev + "]";
    }
}
