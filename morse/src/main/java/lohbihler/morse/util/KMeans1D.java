package lohbihler.morse.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import lohbihler.morse.util.StandardDeviation.MeanAndDeviation;

/**
 * K-means implementation for 1-dimensional integers.
 *
 * @author Matthew
 */
public class KMeans1D {
    public static Set<Centroid> toCentroids(final Collection<Integer> centroids) {
        if (centroids == null) {
            return null;
        }
        final AtomicInteger counter = new AtomicInteger(0);
        return centroids.stream().map(l -> new Centroid(Integer.toString(counter.getAndIncrement()), l))
                .collect(Collectors.toSet());
    }

    public static Map<Centroid, List<Integer>> minDistClusters(final int centroidCount,
            final Set<Centroid> defaultCentroids, final Collection<Integer> points) {
        // Run once with random centroids
        final Map<Centroid, List<Integer>> randomClusters = cluster(points, pickCentroids(points, centroidCount));

        if (defaultCentroids == null) {
            return randomClusters;
        }

        if (defaultCentroids.size() != centroidCount) {
            throw new RuntimeException("Default centroid size is not equal to centroid count");
        }
        final Map<Centroid, List<Integer>> defaultClusters = cluster(points, defaultCentroids);

        // Return the clusters with the minimum distance sum.
        final int randomSum = sumDist(randomClusters);
        final int defaultSum = sumDist(defaultClusters);

        if (randomSum < defaultSum) {
            updateStandardDeviations(randomClusters);
            return randomClusters;
        }
        updateStandardDeviations(defaultClusters);
        return defaultClusters;
    }

    public static Map<Centroid, List<Integer>> minDistClusters(final int centroidCount, final int iterations,
            final Collection<Integer> points) {
        int minSum = Integer.MAX_VALUE;
        Map<Centroid, List<Integer>> minClusters = null;
        for (int i = 0; i < iterations; i++) {
            final Set<Centroid> centroids = pickCentroids(points, centroidCount);
            final Map<Centroid, List<Integer>> clusters = cluster(points, centroids);
            final int sum = sumDist(clusters);

            if (minSum > sum) {
                minSum = sum;
                minClusters = clusters;
            }
        }

        //        System.out.println("count: " + centroidCount + ", sum=" + minSum + ", clusters=" + minClusters);
        //        System.out.println(minSum);
        //        minClusters.forEach((centroid, centroidPoints) -> {
        //            System.out.println("Centroid: " + centroid + ", points: " + centroidPoints.size());
        //        });
        //        System.out.println("count: " + centroidCount + ", sum=" + minSum + ", clusters=" + minClusters);

        updateStandardDeviations(minClusters);
        return minClusters;
    }

    static void updateStandardDeviations(final Map<Centroid, List<Integer>> clusters) {
        for (final Map.Entry<Centroid, List<Integer>> entry : clusters.entrySet()) {
            final MeanAndDeviation mad = StandardDeviation.calculateMeanAndDeviation(entry.getValue());
            if (mad == null) {
                entry.getKey().setStdDev(0);
            } else {
                entry.getKey().setStdDev((int) mad.getDeviation());
            }
        }
    }

    static int sumDist(final Map<Centroid, List<Integer>> clusters) {
        int sum = 0;
        for (final Map.Entry<Centroid, List<Integer>> e : clusters.entrySet()) {
            final Centroid centroid = e.getKey();
            for (final Integer point : e.getValue())
                sum += distance(centroid.getValue(), point);
        }
        return sum;
    }

    static Map<Centroid, List<Integer>> cluster(final Collection<Integer> points, final Set<Centroid> centroids) {
        int maxIterations = 1000;

        // Enter loop
        Map<Centroid, List<Integer>> lastCentroidPoints = null;
        Map<Centroid, List<Integer>> centroidPoints = null;
        while (maxIterations-- > 0) {
            centroidPoints = assignPoints(points, centroids);

            if (lastCentroidPoints != null) {
                // Compare the point lists of the centroids.
                final boolean changed = comparePointLists(lastCentroidPoints, centroidPoints);
                if (!changed)
                    break;
            }

            lastCentroidPoints = centroidPoints;
            fixCentroids(centroidPoints);
            //            centroids = centroidPoints.keySet();
        }

        return centroidPoints;
    }

    static Map<Centroid, List<Integer>> assignPoints(final Collection<Integer> points, final Set<Centroid> centroids) {
        final Map<Centroid, List<Integer>> result = new HashMap<>();
        for (final Centroid centroid : centroids) {
            result.put(centroid, new ArrayList<Integer>());
        }

        for (final Integer point : points) {
            Centroid closestCentroid = null;
            int closestDistance = Integer.MAX_VALUE;
            for (final Centroid centroid : centroids) {
                final int d = distance(point, centroid.getValue());
                if (closestDistance > d) {
                    closestDistance = d;
                    closestCentroid = centroid;
                }
            }

            final List<Integer> clist = result.get(closestCentroid);
            clist.add(point);
        }

        return result;
    }

    static void fixCentroids(final Map<Centroid, List<Integer>> centroidPoints) {
        for (final Map.Entry<Centroid, List<Integer>> e : centroidPoints.entrySet()) {
            if (!e.getValue().isEmpty()) {
                int center = 0;
                for (final Integer point : e.getValue()) {
                    center += point;
                }
                center /= e.getValue().size();
                e.getKey().setValue(center);
            }
        }
    }

    static boolean comparePointLists(final Map<Centroid, List<Integer>> m1, final Map<Centroid, List<Integer>> m2) {
        for (final Map.Entry<Centroid, List<Integer>> e : m1.entrySet()) {
            final List<Integer> pl1 = e.getValue();
            final List<Integer> pl2 = m2.get(e.getKey());

            // If size is different, there is a change.
            if (pl1.size() != pl2.size())
                return true;

            // Point lists should be in the same order, so scan through and check identity.
            for (int i = 0; i < pl1.size(); i++) {
                if (pl1.get(i) != pl2.get(i))
                    return true;
            }
        }

        return false;
    }

    static Set<Centroid> pickCentroids(final Collection<Integer> points, final int clusters) {
        // Find the range
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for (final Integer p : points) {
            if (min > p) {
                min = p;
            }
            if (max < p) {
                max = p;
            }
        }

        // Choose random points within the ranges as the initial centroids.
        final Random random = new Random();
        final Set<Centroid> centroids = new HashSet<>();
        for (int i = 0; i < clusters; i++) {
            final int r = (int) (random.nextDouble() * (max - min) + min);
            centroids.add(new Centroid("" + i, r));
        }

        return centroids;
    }

    static int distance(final Integer l1, final Integer l2) {
        final int diff = l1 - l2;
        if (diff < 0) {
            return -diff;
        }
        return diff;
    }

    /**
     * Poor man's testing. Should be converted to a proper unit test.
     */
    public static void main(final String[] args) throws Exception {
        //        List<Point> points = Loader.load(Loader.test, false);
        //        final List<Integer> points = Loader.load(Loader.profiledPoints, true);

        final int[] values = { 316211, 301178, 92792, 119600, 290961, 298636, 120041, 80093, 79878, 80823, 91442,
                297386, 80074, 110977, 699611, 299693, 81427, 81614, 696435, 310112, 110044, 99851, 89640, 319928,
                689938, 110201, 89869, 89857, 320172, 67073, 89806, 298237, 90313, 299834, 89304, 88583, 119733, 308889,
                100509, 118374, 91382, 680001, 109573, 88695, 290632, 79923, 299668, 299859, 109778, 110186, 299699,
                89990, 100995, 110135, 99706, 78695, 709838, 280508, 79510, 99383, 109833, 278802, 689624, 108136,
                289886, 99751, 299774, 109908, 119935, 299478, 119703, 99800, 99727, 371603, 102583, 295004, 300719,
                121297, 99066, 290209, 99540, 80980, 99797, 99582, 79933, 721100, 309952, 100331, 119851, 89872, 351579,
                709062, 119821, 108075, 300580, 299847, 119751, 119525, 110397, 299638, 310523, 80796, 99612, 110304,
                309273, 109211, 108882, 311299, 108610, 99827, 99993, 89827, 99836, 738745, 319955, 100080, 119775,
                91506, 300094, 738648, 281628, 99821, 81397, 309659, 98589, 98405, 339822, 98190, 109926, 299819, 89120,
                110038, 99791, 299970, 79993, 110035, 129932, 301124, 301078, 108933, 320015, 99220, 350193, 99606,
                88975, 298612, 346791, 109516, 99851, 330480, 91161, 99996, 330076, 139328, 109854, 111062, 99866,
                100720, 739508, 319952, 109609, 93556, 74220, 318261, 739880, 109929, 108830, 299985, 109854, 108504,
                318518, 119914, 109803, 340157, 97257, 302005, 99597, 108453, 749786, 99872, 101158, 100255, 350079,
                319701, 99543, 98387, 309913, 129244, 99727, 777980, 109621, 301389, 78586, 749895, 349819, 109899,
                109745, 118471, 318856, 759095, 119582, 79990, 328569, 110367, 109449, 66246, 320761, 100657, 59724,
                339028, 81210, 338068, 99664, 90440, 320130, 767817, 130823, 119941, 99422, 329934, 109809, 119890,
                318602, 99673, 99881, 119827, 347422, 330535, 128380, 129621, 324768, 118284, 99806, 89836, 120098,
                100228, 749871, 320000, 98676, 99996, 118876, 339916, 769840, 99902, 109253, 310837, 80017, 310964,
                350178, 109896, 120412, 769052, 109350, 89739, 328883, 128878, 99371, 98619, 349970, 108948, 90189,
                771537, 90080, 99987, 349034, 329403, 769949, 320801, 150101, 128084, 121158, 331281, 109872, 341453,
                99627, 319801, 109957, 100032, 339837, 120231, 98882, 771112, 118317, 99984, 329967, 129959, 340036,
                98377, 109905, 119972, 339747, 111116, 101273, 110056, 331274, 338563, 119301, 109187, 329967, 319541,
                139899, 329732, 359946, 111357, 121182, 119984, 359517, 119488, 110382, 109875, 339937, 119887, 89712,
                98417, 118290, 108758, 780142, 320061, 119796, 109935, 109697, 343318, 350206, 110681, 110056, 109960,
                350353, 120056, 129603, 120014, 100588, 99842, 339789, 110434, 119956, 338557, 817274, 100651, 344110,
                130714, 129965, 350366, 801365, 109643, 118758, 120180, 350067, 99911, 119428, 349197, 121179, 330064,
                129950, 100141, 789590, 129793, 110050, 301051, 108981, 90600, 91823, 781284, 111258, 109911, 357558,
                100292, 116907, 331383, 98341, 116493, 129307, 343488, 350661, 120639, 118900, 329795, 120104, 104428,
                130455, 136874, 114516, 810130, 99126, 360039, 118716, 340046, 100065, 119078, 829297, 340006, 110210,
                100047, 110089, 349973, 341129, 109981, 128489, 110443, 829867, 109960, 118244, 119972, 379457, 108679,
                329412, 109920, 121469, 111554, 349641, 818783, 109537, 330018, 99978, 100074, 830308, 118966, 110201,
                340033, 340607, 109630, 139817, 339973, 108048, 150608, 117523, 339967, 348533, 129180, 119661, 108725,
                330043, 798149, 120367, 130023, 110098, 360009, 119455, 99893, 309982, 108534, 110008, 789943, 338349,
                101436, 131246, 103374, 339994, 800692, 111448, 91974, 319611, 332618, 122031, 310094, 335236, 90397,
                131080, 321610, 126813, 101379, 781160, 108133, 110793, 111185, 339979, 80334, 119667, 341443, 118519,
                109733, 178147, 101179, 137801, 108399, 121149, 109978, 778596, 119935, 130295, 110379, 331009, 109120,
                119600, 331431, 102290, 128024, 778977, 91412, 99745, 89923, 302706, 100086, 320357, 97734, 780991,
                130038, 109627, 129890, 349940, 102164, 81059, 358285, 98951, 119914, 339970, 359828, 769952, 348083,
                111647, 100412, 99624, 328261, 329895, 110434, 359719, 88664, 99473, 118278, 133054, 98930, 769535,
                109944, 129507, 340079, 109935, 340085, 108402, 119965, 329973, 99621, 340819, 90545, 89990, 331262,
                129911, 91116, 351120, 350160, 809786, 110165, 100681, 340761, 109072, 341395, 348780, 119787, 109902,
                112145, 791274, 329940, 108634, 89576, 108673, 339946, 329922, 109467, 330172, 121243, 141067, 119962,
                118540, 118924, 760058, 129941, 89673, 339759, 118972, 126967, 89504, 343732, 99727, 91895, 310583,
                100588, 109896, 320088, 110053, 320242, 99960, 109941, 108725, 310064, 100473, 91300, 109899, 748385,
                91158, 110630, 329964, 109763, 110047, 730064, 99833, 109274, 98408, 318448, 109932, 310091, 109857,
                89546, 128649, 304234, 119570, 318530, 101143, 99978, 99772, 290620, 100162, 121644, 89800, 739415,
                318778, 99972, 88758, 120433, 299949, 311320, 90162, 268340, 138214, 81907, 119908, 118785, 99960,
                730423, 70887, 311256, 101415, 100098, 90319, 290429, 119869, 98637, 310052, 120023, 100207, 329813,
                729545, 319584, 78109, 128253, 88634, 310079, 730070, 92502, 99380, 308805, 99975, 308609, 109304,
                89824, 70998, 320254, 99896, 89612, 119830, 700155, 320052, 91487, 119962, 90056, 310003, 81805, 290746,
                101484, 329858, 100035, 119754, 700034, 91294, 129881, 90582, 330656, 109715, 109262, 288533, 119796,
                110192, 720318, 80062, 109866, 100249, 309460, 99935, 299907, 109972, 309961, 118779, 110177, 89824,
                80014, 98390, 319913, 719524, 101777, 109691, 299807, 100047, 109778, 701408, 100180, 299798, 99688,
                99969, 700019, 110171, 299249, 100059, 108483, 281456, 89911, 309898, 99784, 99884, 329871, 99996,
                89640, 319904, 699823, 308907, 109781, 89993, 99981, 308044, 299901, 111040, 299696, 89377, 108405,
                99908, 121240, 91358, 729581, 99923, 90029, 100156, 311148, 299922, 101400, 100274, 106300, 299530,
                89960, 329291, 99905, 89963, 290043, 88577, 99948, 319101, 710167, 310031, 129171, 99929, 100259,
                301193, 280683, 110041, 89727, 109703, 669708, 100056, 88836, 100219, 290306, 80041, 99866, 80195,
                271541, 117390, 290623, 99178, 278594, 119881, 99987, 279961, 729820, 319215, 118377, 87596, 108694,
                289832, 90811, 279826, 118543, 270707, 118984, 100120, 318445, 90014, 107233, 276547, 100509, 108667,
                118401, 99993, 101457, 699889, 300215, 129926, 119271, 111318, 319883, 300139, 108716, 101862, 99667,
                729955, 90207, 106976, 108172, 301015, 109908, 99268, 318548, 99827, 88039, 309569, 99815, 100319,
                100198, 719484, 298612, 90138, 101068, 99751, 299910, 689509, 99446, 107752, 119827, 319934, 99975,
                119917, 280121, 89181, 311241, 100769, 325332, 131149, 741396, 101620, 89957, 338195, 98299, 321027,
                120880, 100956, 121478, 318708, 739967, 119987, 99981, 97943, 309714, 100095, 109911, 309931, 109905,
                109223, 290695, 87988, 90062, 311921, 117825, 331341, 100053, 104365, 309816, 98601, 110192, 320317,
                88115, 97399, 89917, 100298, 98519, 748563, 109941, 329937, 102263, 310375, 91267, 109842, 730124,
                90856, 99458, 331658, 109914, 99911, 98477, 300188, 110883, 300001, 107650, 91478, 80567, 319336,
                729732, 99881, 101342, 320049, 99896, 130343, 289922, 110035, 308932, 729856, 119981, 290058, 128407,
                318753, 99896, 119799, 100014, 728446, 99935, 109869, 319831, 289949, 719940, 301296, 88686, 101219,
                101065, 311435, 278660, 99821, 728461, 110440, 298286, 129192, 121028, 729847, 330275, 109972, 107296,
                101288, 329333, 701550, 80579, 130023, 319994, 106774, 99872, 311130, 91324, 319952, 121300, 101327,
                101137, 311688, 109875, 118782, 111321, 750858, 110135, 75026, 321169, 123441, 349568, 336296, 90186,
                100476, 350483, 89072, 128278, 128734, 98695, 136723, 752304, 108000, 129956, 322537, 817567, 91107,
                108864, 329481, 320483, 770073, 119030, 109890, 350372, 329472, 100213, 327600, 90681, 339279, 89812,
                120002, 323301, 109322, 110669, 350821, 98344, 120044, 121318, 99893, 99712, 789913, 130219, 110014,
                90989, 340634, 338494, 101309, 109899, 119398, 320357, 100416, 320357, 119996, 120095, 329934, 98661,
                117650, 341133, 769825, 320858, 100180, 121463, 100032, 340323, 758548, 110150, 109434, 111454, 340625,
                321329, 110153, 110074, 309708, 121357, 120132, 120261, 320076, 111131, 110041, 110011, 338913, 769895,
                99664, 99923, 350127, 109914, 119504, 99794, 330031, 122103, 122450, 789348, 91342, 321214, 118836,
                101116, 330891, 761927, 99537, 109887, 109531, 331163, 109944, 130449, 329874, 97873, 330003, 100044,
                90319, 100419, 321256, 109676, 110011, 80727, 759194, 322346, 109978, 338817, 100056, 119748, 320333,
                109482, 108969, 275647, 115817, 90123, 321250, 109975, 111104, 99697, 779647, 319868, 100790, 140110,
                758989, 339855, 91336, 120612, 100068, 329786, 128894, 338853, 92281, 308760, 129999, 120995, 826534,
                359689, 120035, 99896, 101940, 329538, 331733, 119848, 99972, 109328, 733533, 100343, 128839, 119443,
                350076, 99866, 310946, 134256, 758763, 101173, 130971, 109359, 320094, 108640, 101077, 139371, 349297,
                120844, 324481, 82937, 349943, 106602, 108990, 351202, 727558, 320505, 97064, 108504, 129621, 347373,
                798759, 99129, 139365, 358430, 119899, 89957, 339849, 118628, 100693, 318784, 101167, 120138, 121529,
                350006, 138842, 129627, 358128, 109914, 111472, 110183, 109993, 99845, 808681, 109984, 359716, 109981,
                130298, 348578, 821160, 330954, 99987, 84981, 132136, 337313, 830999, 121768, 83906, 359864, 129974,
                341462, 339759, 140581, 109600, 828850, 128124, 113275, 339019, 129401, 129987, 112629, 371579, 96185,
                802887, 139609, 102885, 349599, 90026, 581119, 709919 };
        final List<Integer> points = Arrays.stream(values).mapToObj(i -> (Integer) i).collect(Collectors.toList());

        final int iterations = 1;

        //        for (int i = 2; i < 100; i++)
        //            minDistClusters(i, iterations, points);

        //                minDistClusters(2, iterations, points);
        System.out.println(minDistClusters(3, iterations, points));
        //        minDistClusters(4, iterations, points);
        //        minDistClusters(5, iterations, points);
        //        minDistClusters(6, iterations, points);
        //        minDistClusters(7, iterations, points);
        //        minDistClusters(8, iterations, points);
        //        minDistClusters(9, iterations, points);
        //        minDistClusters(10, iterations, points);
        //        minDistClusters(11, iterations, points);
        //        minDistClusters(12, iterations, points);
        //        minDistClusters(13, iterations, points);
        //        minDistClusters(14, iterations, points);
        //        minDistClusters(15, iterations, points);
        //        minDistClusters(16, iterations, points);
        //        minDistClusters(17, iterations, points);
        //        minDistClusters(18, iterations, points);
        //        minDistClusters(19, iterations, points);
        //        minDistClusters(20, iterations, points);
    }
}
