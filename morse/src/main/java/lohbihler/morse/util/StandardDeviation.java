package lohbihler.morse.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class StandardDeviation {

    public static MeanAndDeviation calculateMeanAndDeviation(final double[] values) {
        if (values == null || values.length == 0)
            return null;

        double mean = 0;
        for (int i = 0; i < values.length; i++)
            mean += values[i];
        mean /= values.length;

        double dev = 0;
        double diff;
        for (int i = 0; i < values.length; i++) {
            diff = mean - values[i];
            dev += diff * diff;
        }
        dev /= values.length;
        dev = Math.sqrt(dev);

        return new MeanAndDeviation(mean, dev);
    }

    public static MeanAndDeviation calculateMeanAndDeviation(final int[] values) {
        if (values == null || values.length == 0)
            return null;

        double mean = 0;
        for (int i = 0; i < values.length; i++)
            mean += values[i];
        mean /= values.length;

        double dev = 0;
        double diff;
        for (int i = 0; i < values.length; i++) {
            diff = mean - values[i];
            dev += diff * diff;
        }
        dev /= values.length;
        dev = Math.sqrt(dev);

        return new MeanAndDeviation(mean, dev);
    }

    public static MeanAndDeviation calculateMeanAndDeviation(final Collection<? extends Number> values) {
        if (values == null || values.size() == 0)
            return null;

        double mean = 0;
        for (final Number value : values) {
            mean += value.doubleValue();
        }
        mean /= values.size();

        double dev = 0;
        double diff;
        for (final Number value : values) {
            diff = mean - value.doubleValue();
            dev += diff * diff;
        }
        dev /= values.size();
        dev = Math.sqrt(dev);

        return new MeanAndDeviation(mean, dev);
    }

    public static MeanAndDeviation calculateMeanAndDeviation(final Map<? extends Number, Integer> weightedValues) {
        if (weightedValues == null || weightedValues.size() == 0)
            return null;

        int length = 0;
        double mean = 0;
        for (final Map.Entry<? extends Number, Integer> entry : weightedValues.entrySet()) {
            length += entry.getValue();
            mean += entry.getKey().doubleValue() * entry.getValue();
        }
        mean /= length;

        double dev = 0;
        double diff;
        for (final Map.Entry<? extends Number, Integer> entry : weightedValues.entrySet()) {
            diff = mean - entry.getKey().doubleValue();
            dev += diff * diff * entry.getValue();
        }

        dev /= length;
        dev = Math.sqrt(dev);

        return new MeanAndDeviation(mean, dev);
    }

    public static class MeanAndDeviation {
        private final double mean;
        private final double deviation;

        public MeanAndDeviation(final double mean, final double deviation) {
            this.mean = mean;
            this.deviation = deviation;
        }

        public double getMean() {
            return mean;
        }

        public double getDeviation() {
            return deviation;
        }

        @Override
        public String toString() {
            return "MeanAndDeviation [deviation=" + deviation + ", mean=" + mean + "]";
        }
    }

    public static double getValueProbability(final double mean, final double stdDev, final double value) {
        final int z = (int) (StrictMath.abs(mean - value) / stdDev * 100);
        if (z >= PROBABILITY_TABLE.length) {
            return 0; // TODO should some non-zero value always be returned?
        }
        return PROBABILITY_TABLE[z];
    }

    private static double[] PROBABILITY_TABLE = { 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004,
            0.004, 0.004, 0.004, 0.0039, 0.0039, 0.0039, 0.0039, 0.0039, 0.0039, 0.0039, 0.0039, 0.0039, 0.0039, 0.0039,
            0.0039, 0.0039, 0.0038, 0.0038, 0.0038, 0.0038, 0.0038, 0.0038, 0.0038, 0.0038, 0.0038, 0.0037, 0.0037,
            0.0037, 0.0037, 0.0037, 0.0037, 0.0037, 0.0036, 0.0036, 0.0036, 0.0036, 0.0036, 0.0036, 0.0036, 0.0035,
            0.0035, 0.0035, 0.0035, 0.0035, 0.0034, 0.0034, 0.0034, 0.0034, 0.0034, 0.0033, 0.0033, 0.0033, 0.0033,
            0.0033, 0.0032, 0.0032, 0.0032, 0.0032, 0.0031, 0.0031, 0.0031, 0.0031, 0.0031, 0.0031, 0.003, 0.003, 0.003,
            0.0029, 0.0029, 0.0029, 0.0029, 0.0029, 0.0028, 0.0028, 0.0028, 0.0028, 0.0028, 0.0027, 0.0027, 0.0027,
            0.0026, 0.0026, 0.0026, 0.0026, 0.0026, 0.0025, 0.0025, 0.0025, 0.0025, 0.0024, 0.0024, 0.0024, 0.0023,
            0.0023, 0.0023, 0.0023, 0.0023, 0.0022, 0.0022, 0.0022, 0.0022, 0.0022, 0.0021, 0.0021, 0.0021, 0.002,
            0.002, 0.002, 0.002, 0.002, 0.0019, 0.0019, 0.0019, 0.0019, 0.0018, 0.0018, 0.0018, 0.0018, 0.0017, 0.0017,
            0.0017, 0.0017, 0.0017, 0.0016, 0.0016, 0.0016, 0.0016, 0.0015, 0.0015, 0.0015, 0.0015, 0.0015, 0.0015,
            0.0014, 0.0014, 0.0014, 0.0014, 0.0013, 0.0013, 0.0013, 0.0013, 0.0013, 0.0012, 0.0012, 0.0012, 0.0012,
            0.0012, 0.0012, 0.0011, 0.0011, 0.0011, 0.0011, 0.0011, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001,
            0.0009, 0.0009, 0.0009, 0.0009, 0.0009, 0.0009, 0.0008, 0.0008, 0.0008, 0.0008, 0.0008, 0.0008, 0.0008,
            0.0007, 0.0007, 0.0007, 0.0007, 0.0007, 0.0007, 0.0007, 0.0006, 0.0006, 0.0006, 0.0006, 0.0006, 0.0006,
            0.0006, 0.0006, 0.0006, 0.0005, 0.0005, 0.0005, 0.0005, 0.0005, 0.0005, 0.0005, 0.0005, 0.0005, 0.0005,
            0.0004, 0.0004, 0.0004, 0.0004, 0.0004, 0.0004, 0.0004, 0.0004, 0.0004, 0.0004, 0.0004, 0.0004, 0.0003,
            0.0003, 0.0003, 0.0003, 0.0003, 0.0003, 0.0003, 0.0003, 0.0003, 0.0003, 0.0003, 0.0003, 0.0003, 0.0003,
            0.0003, 0.0002, 0.0002, 0.0002, 0.0002, 0.0002, 0.0002, 0.0002, 0.0002, 0.0002, 0.0002, 0.0002, 0.0002,
            0.0002, 0.0002, 0.0002, 0.0002, 0.0002, 0.0002, 0.0002, 0.0002, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001,
            0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001,
            0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001,
            0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001,
            0.0001, 0.0001, 0.0001, };

    public static void main(final String[] args) {
        System.out.println(calculateMeanAndDeviation(new double[] { 2, 4, 4, 4, 5, 5, 7, 9 }));

        final Map<Double, Integer> weightedValues = new HashMap<>();
        weightedValues.put(2D, 1);
        weightedValues.put(4D, 3);
        weightedValues.put(5D, 2);
        weightedValues.put(7D, 1);
        weightedValues.put(9D, 1);
        System.out.println(calculateMeanAndDeviation(weightedValues));
    }
}
