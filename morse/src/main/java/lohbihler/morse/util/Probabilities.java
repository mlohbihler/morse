package lohbihler.morse.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * A list of values and their associated probabilities.
 */
public class Probabilities<T> {
    private final Map<T, Probability<T>> probabilities = new HashMap<>();

    public void add(final T value, final double probability) {
        add(new Probability<>(value, probability));
    }

    public void add(final Probability<T> p) {
        probabilities.put(p.getValue(), p);
    }

    public void normalize() {
        final double sum = probabilities.values().stream().reduce(new Double(0),
                (total, p) -> total + p.getProbability(), Double::sum);
        if (sum > 0) {
            probabilities.values().stream().forEach(p -> p.setProbability(p.getProbability() / sum));
        }
    }

    public double getProbability(final String value) {
        final Probability<T> p = probabilities.get(value);
        if (p == null) {
            return 0;
        }
        return p.getProbability();
    }

    public int size() {
        return probabilities.size();
    }

    public Probability<T> getMostLikely() {
        return Collections.max(probabilities.values(),
                (a, b) -> Double.compare(a.getProbability(), b.getProbability()));
    }

    public static <T> Probabilities<T> merge(final Probabilities<T> a, final Probabilities<T> b) {
        final Probabilities<T> merged = new Probabilities<>();

        a.probabilities.values().forEach(e -> merged.add(e.getValue(), e.getProbability()));
        b.probabilities.values().forEach(e -> {
            final Probability<T> existing = merged.probabilities.get(e.getValue());
            if (existing == null) {
                merged.add(e.getValue(), e.getProbability());
            } else {
                existing.setProbability(existing.getProbability() + e.getProbability());
            }
        });

        merged.normalize();

        return merged;
    }

    @Override
    public String toString() {
        return "Probabilities [probabilities=" + probabilities + "]";
    }
}
