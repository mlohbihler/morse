package lohbihler.morse.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Counter<T> {
    private final Map<T, Integer> counts = new HashMap<>();
    private int total;

    public void add(final T item) {
        add(item, 1);
    }

    public void add(final T item, final int amount) {
        final Integer count = getCount(item);
        counts.put(item, count + amount);
        total += 1;
    }

    public Map<T, Integer> getCounts() {
        return counts;
    }

    public List<Map.Entry<T, Integer>> getSortedCounts(final boolean ascending) {
        final List<Map.Entry<T, Integer>> list = new ArrayList<>();
        for (final Map.Entry<T, Integer> entry : counts.entrySet()) {
            list.add(entry);
        }
        Collections.sort(list, (o1, o2) -> {
            if (ascending) {
                return o1.getValue() - o2.getValue();
            }
            return o2.getValue() - o1.getValue();
        });
        return list;
    }

    public void printSortedCounts(final boolean ascending) {
        for (final Map.Entry<T, Integer> e : getSortedCounts(ascending)) {
            System.out.println(e.getKey() + "\t" + e.getValue());
        }
    }

    public int getTotal() {
        return total;
    }

    public int size() {
        return counts.size();
    }

    public int getCount(final T key) {
        final Integer count = counts.get(key);
        if (count == null) {
            return 0;
        }
        return count;
    }

    @Override
    public String toString() {
        return "Counter [counts=" + counts + ", total=" + total + "]";
    }
}
