package lohbihler.morse.util;

/**
 * The probability of a given value. The probability is mutable so that it can be normalized.
 *
 * @param <T>
 */
public class Probability<T> {
    private final T value;
    private double probability;

    public Probability(final T value, final double probability) {
        this.value = value;
        this.probability = probability;
    }

    public T getValue() {
        return value;
    }

    public double getProbability() {
        return probability;
    }

    public void setProbability(final double probability) {
        this.probability = probability;
    }

    @Override
    public String toString() {
        return "Probability [value=" + value + ", probability=" + probability + "]";
    }
}
