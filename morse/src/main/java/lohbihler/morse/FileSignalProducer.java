package lohbihler.morse;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.FileReader;
import java.io.IOException;
import java.time.Clock;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileSignalProducer implements Closeable {
    static final Logger LOG = LoggerFactory.getLogger(FileSignalProducer.class);

    private final AtomicBoolean target;

    private final BufferedReader input;
    private boolean done;
    private long nextOffset;
    private String nextAction;

    public FileSignalProducer(final AtomicBoolean target, final String inputFile) throws IOException {
        this.target = target;
        input = new BufferedReader(new FileReader(inputFile));
        advance();
    }

    @Override
    public void close() throws IOException {
        input.close();
    }

    public boolean isDone() {
        return done;
    }

    public void execute(final Clock clock) throws IOException {
        final long now = clock.millis();
        while (!done && nextOffset <= now) {
            if (nextAction.equals("+")) {
                target.set(true);
            } else if (nextAction.equals("-")) {
                target.set(false);
            } else {
                throw new RuntimeException("Invalid action at " + nextOffset + ": '" + nextAction + "'");
            }
            advance();
        }
    }

    private void advance() throws IOException {
        while (true) {
            final String line = input.readLine();
            if (line == null || line.trim().equals("")) {
                done = true;
                break;
            } else if (line.startsWith("!")) {
                continue;
            }

            // Split line on whitespace
            final String[] parts = line.split("\\s+");
            nextOffset += Long.parseLong(parts[0]);
            nextAction = parts[1];
            break;
        }
    }
}
