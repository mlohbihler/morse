package lohbihler.morse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import lohbihler.morse.util.Probabilities;

public enum MorseCode {
    DOT(1), // dot - a single time unit
    DASH(3), // dash - three time units
    SYMBOL_SEPARATION(1), // separation between dots and dashes - silence equal to one dot
    LETTER_SEPARATION(3), // separation between letters - silence equal to three dots
    WORD_SEPARATION(7), // separation between words - silence equal to 7 dots
    ;

    public final int beats;

    private MorseCode(final int beats) {
        this.beats = beats;
    }

    public static final MorseCode[][] SYMBOLS = { //
            { WORD_SEPARATION }, // space
            { DASH, DOT, DASH, DOT, DASH, DASH }, // !
            { DOT, DASH, DOT, DOT, DASH, DOT }, // "
            null, // #
            { DOT, DOT, DOT, DASH, DOT, DOT, DASH }, // $
            null, // %
            { DOT, DASH, DOT, DOT, DOT }, // &
            { DOT, DASH, DASH, DASH, DASH, DOT }, // '
            { DASH, DOT, DASH, DASH, DOT }, // (
            { DASH, DOT, DASH, DASH, DOT, DASH }, // )
            null, // *
            { DOT, DASH, DOT, DASH, DOT }, // +
            { DASH, DASH, DOT, DOT, DASH, DASH }, // ,
            { DASH, DOT, DOT, DOT, DOT, DASH }, // -
            { DOT, DASH, DOT, DASH, DOT, DASH }, // .
            { DASH, DOT, DOT, DASH, DOT }, // /
            { DASH, DASH, DASH, DASH, DASH }, // 0
            { DOT, DASH, DASH, DASH, DASH }, // 1
            { DOT, DOT, DASH, DASH, DASH }, // 2
            { DOT, DOT, DOT, DASH, DASH }, // 3
            { DOT, DOT, DOT, DOT, DASH }, // 4
            { DOT, DOT, DOT, DOT, DOT }, // 5
            { DASH, DOT, DOT, DOT, DOT }, // 6
            { DASH, DASH, DOT, DOT, DOT }, // 7
            { DASH, DASH, DASH, DOT, DOT }, // 8
            { DASH, DASH, DASH, DASH, DOT }, // 9
            { DASH, DASH, DASH, DOT, DOT, DOT }, // :
            { DASH, DOT, DASH, DOT, DASH, DOT }, // ;
            null, // <
            { DASH, DOT, DOT, DOT, DASH }, // =
            null, // >
            { DOT, DOT, DASH, DASH, DOT, DOT }, // ?
            { DOT, DASH, DASH, DOT, DASH, DOT }, // @
            { DOT, DASH }, // A
            { DASH, DOT, DOT, DOT }, // B
            { DASH, DOT, DASH, DOT }, // C
            { DASH, DOT, DOT }, // D
            { DOT }, // E
            { DOT, DOT, DASH, DOT }, // F
            { DASH, DASH, DOT }, // G
            { DOT, DOT, DOT, DOT }, // H
            { DOT, DOT }, // I
            { DOT, DASH, DASH, DASH }, // J
            { DASH, DOT, DASH }, // K
            { DOT, DASH, DOT, DOT }, // L
            { DASH, DASH }, // M
            { DASH, DOT }, // N
            { DASH, DASH, DASH }, // O
            { DOT, DASH, DASH, DOT }, // P
            { DASH, DASH, DOT, DASH }, // Q
            { DOT, DASH, DOT }, // R
            { DOT, DOT, DOT }, // S
            { DASH }, // T
            { DOT, DOT, DASH }, // U
            { DOT, DOT, DOT, DASH }, // V
            { DOT, DASH, DASH }, // W
            { DASH, DOT, DOT, DASH }, // X
            { DASH, DOT, DASH, DASH }, // Y
            { DASH, DASH, DOT, DOT }, // Z
            null, // [
            null, // \
            null, // ]
            null, // ^
            { DOT, DOT, DASH, DASH, DOT, DASH }, // _
    };

    // Sorted
    //    { DOT }, // E
    //    { DOT, DOT }, // I
    //    { DOT, DOT, DOT }, // S
    //    { DOT, DOT, DOT, DOT }, // H
    //    { DOT, DOT, DOT, DOT, DOT }, // 5
    //    { DOT, DOT, DOT, DOT, DASH }, // 4
    //    { DOT, DOT, DOT, DASH }, // V
    //    { DOT, DOT, DOT, DASH, DOT, DOT, DASH }, // $
    //    { DOT, DOT, DOT, DASH, DASH }, // 3
    //    { DOT, DOT, DASH }, // U
    //    { DOT, DOT, DASH, DOT }, // F
    //    { DOT, DOT, DASH, DASH, DOT, DOT }, // ?
    //    { DOT, DOT, DASH, DASH, DOT, DASH }, // _
    //    { DOT, DOT, DASH, DASH, DASH }, // 2
    //    { DOT, DASH }, // A
    //    { DOT, DASH, DOT }, // R
    //    { DOT, DASH, DOT, DOT }, // L
    //    { DOT, DASH, DOT, DOT, DOT }, // &
    //    { DOT, DASH, DOT, DOT, DASH, DOT }, // "
    //    { DOT, DASH, DOT, DASH, DOT }, // +
    //    { DOT, DASH, DOT, DASH, DOT, DASH }, // .
    //    { DOT, DASH, DASH }, // W
    //    { DOT, DASH, DASH, DOT }, // P
    //    { DOT, DASH, DASH, DOT, DASH, DOT }, // @
    //    { DOT, DASH, DASH, DASH }, // J
    //    { DOT, DASH, DASH, DASH, DASH }, // 1
    //    { DOT, DASH, DASH, DASH, DASH, DOT }, // '
    //    { DASH }, // T
    //    { DASH, DOT }, // N
    //    { DASH, DOT, DOT }, // D
    //    { DASH, DOT, DOT, DOT }, // B
    //    { DASH, DOT, DOT, DOT, DOT }, // 6
    //    { DASH, DOT, DOT, DOT, DOT, DASH }, // -
    //    { DASH, DOT, DOT, DOT, DASH }, // =
    //    { DASH, DOT, DOT, DASH }, // X
    //    { DASH, DOT, DOT, DASH, DOT }, // /
    //    { DASH, DOT, DASH }, // K
    //    { DASH, DOT, DASH, DOT }, // C
    //    { DASH, DOT, DASH, DOT, DASH, DOT }, // ;
    //    { DASH, DOT, DASH, DOT, DASH, DASH }, // !
    //    { DASH, DOT, DASH, DASH }, // Y
    //    { DASH, DOT, DASH, DASH, DOT }, // (
    //    { DASH, DOT, DASH, DASH, DOT, DASH }, // )
    //    { DASH, DASH }, // M
    //    { DASH, DASH, DOT }, // G
    //    { DASH, DASH, DOT, DOT }, // Z
    //    { DASH, DASH, DOT, DOT, DOT }, // 7
    //    { DASH, DASH, DOT, DOT, DASH, DASH }, // ,
    //    { DASH, DASH, DOT, DASH }, // Q
    //    { DASH, DASH, DASH }, // O
    //    { DASH, DASH, DASH, DOT, DOT }, // 8
    //    { DASH, DASH, DASH, DOT, DOT, DOT }, // :
    //    { DASH, DASH, DASH, DASH, DOT }, // 9
    //    { DASH, DASH, DASH, DASH, DASH }, // 0

    static Node root;
    static {
        // Construct the binary search tree.
        root = new Node();
        for (int i = 0; i < SYMBOLS.length; i++) {
            final MorseCode[] symbols = SYMBOLS[i];
            if (symbols != null && symbols[0] != WORD_SEPARATION) {
                Node node = root;
                final Character character = (char) (i + 32);
                for (final MorseCode symbol : symbols) {
                    if (symbol == DOT) {
                        if (node.dots == null) {
                            node.dots = new Node();
                        }
                        node = node.dots;
                    } else if (symbol == DASH) {
                        if (node.dashes == null) {
                            node.dashes = new Node();
                        }
                        node = node.dashes;
                    } else {
                        throw new RuntimeException("Invalid symbols: " + Arrays.toString(symbols));
                    }
                }
                node.character = character;
            }
        }
    }

    public static List<Character> characterMatches(final Collection<MorseCode> symbols) {
        final List<Character> matches = new ArrayList<>();

        final Node node = getNode(symbols);
        if (node != null) {
            addCharacters(node, matches);
        }

        return matches;
    }

    public static Probabilities<MorseCode> nextSilence(final Collection<MorseCode> symbols) {
        final Probabilities<MorseCode> probs = new Probabilities<>();
        final Node node = getNode(symbols);
        if (node != null) {
            if (node.character != null) {
                probs.add(LETTER_SEPARATION, 1);
                probs.add(WORD_SEPARATION, 1);
            }

            final int count = countCharacters(node.dots) + countCharacters(node.dashes);
            if (count > 0) {
                probs.add(SYMBOL_SEPARATION, count);
            }
        }
        probs.normalize();
        return probs;
    }

    public static Probabilities<MorseCode> nextDotDash(final Collection<MorseCode> symbols) {
        final Probabilities<MorseCode> probs = new Probabilities<>();
        final Node node = getNode(symbols);
        if (node != null) {
            if (node.dots != null) {
                probs.add(DOT, countCharacters(node.dots));
            }
            if (node.dashes != null) {
                probs.add(DASH, countCharacters(node.dashes));
            }
        }
        probs.normalize();
        return probs;
    }

    private static int countCharacters(final Node node) {
        if (node == null) {
            return 0;
        }
        int count = 0;
        if (node.character != null) {
            count++;
        }
        if (node.dots != null) {
            count += countCharacters(node.dots);
        }
        if (node.dashes != null) {
            count += countCharacters(node.dashes);
        }
        return count;
    }

    private static void addCharacters(final Node node, final List<Character> characters) {
        if (node.character != null) {
            characters.add(node.character);
        }
        if (node.dots != null) {
            addCharacters(node.dots, characters);
        }
        if (node.dashes != null) {
            addCharacters(node.dashes, characters);
        }
    }

    public static Character getCharacter(final Collection<MorseCode> symbols) {
        final Node node = getNode(symbols);
        if (node != null) {
            return node.character;
        }
        return null;
    }

    private static Node getNode(final Collection<MorseCode> symbols) {
        Node node = root;
        for (final MorseCode code : symbols) {
            if (code == DOT) {
                node = node.dots;
            } else if (code == DASH) {
                node = node.dashes;
            } else {
                node = null;
            }
            if (node == null) {
                return null;
            }
        }
        return node;
    }

    public static MorseCode[] getSymbols(final char c) {
        if (c < 32)
            return null;
        if (c < 96)
            return SYMBOLS[c - 32];
        if (c < 97)
            return null;
        if (c < 123)
            return SYMBOLS[c - 64];
        return null;
    }

    static class Node {
        Character character;
        Node dots;
        Node dashes;

        @Override

        public String toString() {
            return "Node [" + character + "]";
        }
    }
}
