package lohbihler.morse;

import java.time.Clock;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lohbihler.morse.util.Probabilities;

/**
 * TODO all letters that match the current symbol list are given equal weighting. They should maybe use a probability
 * weighting instead.
 *
 * TODO add a levenshtein distance for morse code patterns.
 * LevenshteinDistance.getDefaultInstance().apply(left, right)
 */
public class SymbolChangeListener implements ChangeListener<MorseCode> {
    static final Logger LOG = LoggerFactory.getLogger(SymbolChangeListener.class);

    private final List<MorseCode> symbols = new ArrayList<>();
    private final Map<Character, Double> letterPredictions = new HashMap<>();

    private final List<ChangeListener<Character>> listeners = new ArrayList<>();

    private MorseCode lastValue = null;

    public void addListener(final ChangeListener<Character> listener) {
        listeners.add(listener);
    }

    @Override
    public void change(final Clock clock, final MorseCode value) {
        switch (value) {
        case DOT:
        case DASH:
            symbols.add(value);
            //            updatePredictions();
            break;

        case LETTER_SEPARATION:
        case WORD_SEPARATION:
            // Determine the letter. If there is no match we should use the equivalent of a Levenshtein distance to
            // find the most likely match.
            final Character c = MorseCode.getCharacter(symbols);

            // Emit the detected letter.
            listeners.forEach(l -> l.change(clock, c));

            symbols.clear();
            //            updatePredictions();

            if (value == MorseCode.WORD_SEPARATION) {
                // Emit the word separation
                listeners.forEach(l -> l.change(clock, ' '));
            }
            break;

        case SYMBOL_SEPARATION:
        default:
            // no op
            break;
        }

        lastValue = value;
    }

    @Override
    public Probabilities<MorseCode> getPredictions(final Clock clock) {
        if (lastValue == MorseCode.DOT || lastValue == MorseCode.DASH) {
            // Predict a silence type.
            return MorseCode.nextSilence(symbols);
        }

        // Predict a dot or dash.
        return MorseCode.nextDotDash(symbols);
    }
    //
    //    private void updatePredictions() {
    //        letterPredictions.clear();
    //        final List<Character> matches = MorseCode.characterMatches(symbols);
    //        matches.forEach(c -> letterPredictions.put(c, 1D / matches.size()));
    //        //        if (letterPredictions.isEmpty()) {
    //        //            LOG.warn("Letter predictions is empty");
    //        //        }
    //    }
}
