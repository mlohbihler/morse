package lohbihler.morse;

import java.time.Clock;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lohbihler.morse.util.Probabilities;

public class LetterChangeListener implements ChangeListener<Character> {
    static final Logger LOG = LoggerFactory.getLogger(LetterChangeListener.class);

    private final List<Character> letters = new ArrayList<>();

    @Override
    public void change(final Clock clock, final Character value) {
        Character ch = value;
        if (ch == null) {
            ch = '?';
        }
        if (ch == ' ') {
            System.out.println("Received word: " + letters.stream().reduce("", (s, c) -> s + c, (s1, s2) -> s1 + s2));
            letters.clear();
            // TODO Emit word to higher level?
        } else {
            letters.add(ch);
        }
    }

    @Override
    public Probabilities<Character> getPredictions(final Clock clock) {
        // To be similar to the symbol change listener, we would know a list of possible words, and use such matches
        // to return letter probabilities.
        return new Probabilities<>();
    }
}
