package lohbihler.morse;

import java.time.Clock;

import lohbihler.morse.util.Probabilities;

public interface ChangeListener<T> {
    void change(Clock clock, T value);

    Probabilities<T> getPredictions(Clock clock);
}
